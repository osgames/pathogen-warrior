
Ludum Dare 4th 48 hour Game Development Competition
Entry by tonic
http://jet.ro

Theme:      "Infection"
Entry name: "Pathogen Warrior"


Instructions:

Horrid pandemic plagues are threatening the world!
You are the Pathogen Warrior. Your task is to save the world.

Match the pathogens to hex-vaccines. New sample of the pathogen can be
fetched by clicking the problem spot on the world map. You can also
press the space key to fetch a new sample. Drag with left mouse button
to move the ball compounds on the hex map. Drag with right mouse
button to scroll the map. You can also scroll the map with arrow keys.
You can create a link by clicking an empty hexagon or rotate a link by
clicking an existing link.

After you match the pathogen model structure on the hexagon map you
are presented with a new challenge with increasing difficulty. For
each successful vaccine you will be rewarded with a score based on how
fast you accomplished the task.


Version 1.1.1 updates:

* Updated music file.
* Added Linux Makefile (contributed by Ion).
* Added license notices to all source files.

Version 1.1.0 Updates:

* Linux executable is included in the package and the source is
  now verified to correctly compile on Linux.
* Added brightness to graphics to get ball colors to match better and
  to make gameplay easier on brightness level of an average monitor.
* Dragging a ball doesn't scramble the field anymore. New behavior is
  so that when you start to drag a node, it will affect the hexagon
  map only when you release the mouse button. The contents of drag
  start and end node are then switched.
* Added a bar which shows remaining time more clearly.


TODO items/ideas:

* Construct new links between existing nearby balls if there is any.
* Hiscore list.
* Option to use differing shapes than balls and vary brightness of them
  but not colors. This would make the game easier to play for color
  blind people.

--
$Id: README.txt,v 1.7 2004/07/14 15:37:18 tonic Exp $
