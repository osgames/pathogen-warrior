# Pathogen Warrior
#
# Linux Makefile
# Originally contributed by Ion.
#
# $Id: Makefile,v 1.3 2004/07/14 18:24:12 tonic Exp $
# $Revision: 1.3 $

sources := $(wildcard src/*.cpp)
objects := $(sources:%.cpp=%.o)

all: pathogen

CXXFLAGS := $(shell sdl-config --cflags)
LDFLAGS := $(shell sdl-config --libs) -lSDL_image -lSDL_mixer -lGLU -lGL

pathogen: $(objects)
	$(CXX) $(LDFLAGS) -o $@ $^

.PHONY: clean
clean:
	$(RM) pathogen $(objects)
