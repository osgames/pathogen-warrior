/* Pathogen Warrior
 * Copyright 2004 Jetro Lauha - http://iki.fi/jetro/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: Font.h,v 1.3 2004/07/14 15:37:09 tonic Exp $
 * $Revision: 1.3 $
 */

#ifndef FONT_H_INCL
#define FONT_H_INCL


// default alignment is TOP+LEFT, use these flags to change the alignment
enum FONT_ALIGN
{
    FONT_ALIGN_HORIZ_CENTER = 1 << 0,
    FONT_ALIGN_RIGHT        = 1 << 1,
    FONT_ALIGN_VERT_CENTER  = 1 << 2,
    FONT_ALIGN_BOTTOM       = 1 << 3,
};


class Font
{
public:

    Font(char *filename, const char *fontstr = NULL);
    virtual ~Font();

    void reload();

    void setCharacterSpacing(INT spacing) { mSpacing = spacing; }
    INT getCharacterSpacing() { return mSpacing; }
    UINT getFontHeight() { return mFontHeight; }
    INT32 getStringWidth(const char *str);

    void setDrawImageFunc(void (*func)(const IMAGE &img, FLOAT x, FLOAT y,
                                       FLOAT r, FLOAT g, FLOAT b, FLOAT a));
    void drawString(INT32 x, INT32 y, UINT32 flags, const char *str,
                    GLfloat r = 1, GLfloat g = 1, GLfloat b = 1, GLfloat a = 1);

private:

    INT mLoaded;
    char mFilename[256];
    UINT8 mCharMap[256];
    IMAGE mLetter[256];
    UINT mChars;
    INT mSpacing;
    UINT mFontHeight;

    void buildCharMap(const char *fontstr);

    void (*drawImage)(const IMAGE &img, FLOAT x, FLOAT y, FLOAT r, FLOAT g, FLOAT b, FLOAT a);

};


#endif // !FONT_H_INCL
