/* Pathogen Warrior
 * Copyright 2004 Jetro Lauha - http://iki.fi/jetro/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: TitleState.h,v 1.2 2004/07/14 15:37:09 tonic Exp $
 * $Revision: 1.2 $
 */

#ifndef TITLESTATE_H_INCL
#define TITLESTATE_H_INCL


class TitleState : public State
{
public:

    TitleState() {}
    virtual ~TitleState() {}

    virtual BOOL update(UINT32 time, App::KeyEventList &keyEvents);
    virtual void render(SDL_Surface *screen);

    virtual void onKeyDown(const SDLKey &key);
    virtual void onMouseDown(const SDL_MouseButtonEvent &event);

protected:

    void gameStart();

    UINT32 mTime;
};


#endif // !TITLESTATE_H_INCL
