/* Pathogen Warrior
 * Copyright 2004 Jetro Lauha - http://iki.fi/jetro/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: State.h,v 1.7 2004/07/14 15:37:09 tonic Exp $
 * $Revision: 1.7 $
 */

#ifndef STATE_H_INCL
#define STATE_H_INCL


class State
{
public:

    State() : mFPS(5) {}
    virtual ~State() {}

    virtual BOOL init() { return TRUE; }
    virtual void deinit() {}

    UINT32 getUpdateFPS() { return mFPS; }

    virtual BOOL update(UINT32 time, App::KeyEventList &keyEvents) = 0;
    virtual void render(SDL_Surface *screen) = 0;

    virtual void onActivate(UINT32 /*tick*/) {}
    virtual void onDeactivate() {}

    virtual void onKeyDown(const SDLKey &key) {}
    virtual void onKeyUp(const SDLKey &key) {}
    virtual void onMouseDown(const SDL_MouseButtonEvent &event) {}
    virtual void onMouseUp(const SDL_MouseButtonEvent &event) {}
    virtual void onMouseMotion(const SDL_MouseMotionEvent &event) {}

protected:

    UINT32 mFPS;
};


#endif // !STATE_H_INCL
