/* Pathogen Warrior
 * Copyright 2004 Jetro Lauha - http://iki.fi/jetro/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: GameApp.cpp,v 1.9 2004/07/14 15:37:09 tonic Exp $
 * $Revision: 1.9 $
 */

#include "main.h"


GameApp::GameApp() :
    mBuildState(NULL), mTitleState(NULL), mFont(NULL)
{
}


GameApp::~GameApp()
{
    if (mBuildState != NULL)
        mBuildState->deinit();
    delete mBuildState;
    if (mTitleState != NULL)
        mTitleState->deinit();
    delete mTitleState;
}


BOOL GameApp::init()
{
    BOOL result = App::init();

    if (!result)
        return result;

    mFont = new Font("data/font.png");

    mBuildState = new BuildState;
    mBuildState->init();

    mTitleState = new TitleState;
    mTitleState->init();

    if (mBuildState == NULL || mTitleState == NULL)
        return FALSE;
    setState(GS_TITLE);

    return TRUE;
}


char * GameApp::getCaption()
{
    return CAPTION_STR;
}


State * GameApp::getStateHandler(INT state)
{
    switch (state)
    {
    case GS_TITLE:
        return mTitleState;
    case GS_BUILD:
        return mBuildState;
    }
    return NULL;
}
