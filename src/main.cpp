/* Pathogen Warrior
 * Copyright 2004 Jetro Lauha - http://iki.fi/jetro/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: main.cpp,v 1.4 2004/07/14 15:37:09 tonic Exp $
 * $Revision: 1.4 $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "main.h"
#include "GameApp.h"


static SDL_Surface *sScreen;


static SDL_Surface * setVideoMode(int width, int height, int bpp, int fullscreen, int fsaa)
{
    Uint32 videoFlags = 0;
    int rSize, gSize, bSize;

    switch (bpp)
    {
    case 8:
        rSize = 3;
        gSize = 3;
        bSize = 2;
    case 15:
    case 16:
        rSize = 5;
        gSize = 5;
        bSize = 5;
        break;
    default:
        rSize = 8;
        gSize = 8;
        bSize = 8;
        break;
    }
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, rSize);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, gSize);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, bSize);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    if (fsaa)
    {
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, fsaa);
    }

    videoFlags |= SDL_DOUBLEBUF;
    videoFlags |= SDL_OPENGL;
    if (fullscreen)
        videoFlags |= SDL_FULLSCREEN;

    return SDL_SetVideoMode(width, height, bpp, videoFlags);
}


typedef struct {
    int bpp;
    int fullscreen;
} VIDEO_SETTINGS;

static VIDEO_SETTINGS sVideoSettings[] =
{
    { 32, 0 },
    { 24, 0 },
    { 16, 0 },
    { 32, 1 },
    { 24, 1 },
    { 16, 1 },
    { 8, 0 },
    { 8, 1 },
    { 32, 0 }
};
#define SVIDEOSETTINGS_COUNT (sizeof(sVideoSettings) / sizeof(sVideoSettings[0]))


int main(int argc, char *argv[])
{
    int a;

    if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO |
                 SDL_INIT_TIMER | SDL_INIT_NOPARACHUTE) < 0)
    {
        errorMessage("Unable to init SDL", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    atexit(SDL_Quit);

    sScreen = NULL;
    for (a = 0; a < SVIDEOSETTINGS_COUNT; ++a)
    {
        VIDEO_SETTINGS *vs = &sVideoSettings[a];
        sScreen = setVideoMode(WIDTH, HEIGHT, vs->bpp, vs->fullscreen, 0);
        if (sScreen != NULL)
            break;
    }
    if (sScreen == NULL)
    {
        errorMessage("Unable to set video mode", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    GameApp *app = new GameApp;
    BOOL result = app->init();

    initSound();

    if (result)
        app->run();

    deinitSound();

    return EXIT_SUCCESS;
}
