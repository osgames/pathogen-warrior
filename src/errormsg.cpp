/* Pathogen Warrior
 * Copyright 2004 Jetro Lauha - http://iki.fi/jetro/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: errormsg.cpp,v 1.1 2004/07/14 18:23:57 tonic Exp $
 * $Revision: 1.1 $
 */

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "errormsg.h"


static void errorBox(char *title, char *msg, va_list ap)
{
#ifdef WIN32
    char s[1000];
    vsprintf(s,msg,ap);
    MessageBox(0, s, title, MB_OK | MB_APPLMODAL | MB_ICONEXCLAMATION);
#else
    fflush(stderr);
    fflush(stdout);
    fprintf(stderr, "\n%s: ", title);
    vfprintf(stderr, msg, ap);
    fprintf(stderr, "\n");
    fflush(stderr);
#endif
}


void errorMessage(char *title, char *msg, ...)
{
    va_list ap;
    va_start(ap, msg);
    errorBox(title, msg, ap);
}
