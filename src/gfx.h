/* Pathogen Warrior
 * Copyright 2004 Jetro Lauha - http://iki.fi/jetro/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: gfx.h,v 1.7 2004/07/14 15:37:09 tonic Exp $
 * $Revision: 1.7 $
 */

#ifndef GFX_H_INCL
#define GFX_H_INCL


typedef struct
{
    GLuint glid;
    GLfloat uv[4];
    int w, h;
} IMAGE;


extern void clear(float r, float g, float b);

extern void pushGLState();
extern void popGLState();

extern void enter2DMode(INT viewportWidth, INT viewportHeight);
extern void leave2DMode();

extern GLuint SDL_GL_LoadTexture(SDL_Surface *surface, GLfloat *texcoord,
                                 SDL_Rect *srcrect = NULL);
extern IMAGE loadImage(char *filename);
extern void freeImage(const IMAGE &image);

extern void drawImage(const IMAGE &img, FLOAT x = 0, FLOAT y = 0,
                      FLOAT r = 1, FLOAT g = 1,
                      FLOAT b = 1, FLOAT a = 1);
extern void drawImageScaled(const IMAGE &img,
                            FLOAT x = 0, FLOAT y = 0,
                            FLOAT xScale = 1, FLOAT yScale = 1,
                            FLOAT r = 1, FLOAT g = 1,
                            FLOAT b = 1, FLOAT a = 1);
extern void drawImageAdditive(const IMAGE &img,
                              FLOAT x = 0, FLOAT y = 0, FLOAT angle = 0,
                              FLOAT r = 1, FLOAT g = 1,
                              FLOAT b = 1, FLOAT a = 1);
extern void drawImageRotatedCenteredScaled(const IMAGE &img,
                                           FLOAT x = 0, FLOAT y = 0,
                                           FLOAT angle = 0, FLOAT scale = 1,
                                           FLOAT r = 1, FLOAT g = 1,
                                           FLOAT b = 1, FLOAT a = 1);
extern void drawImageRotatedCenteredScaledAdditive(const IMAGE &img,
                                                   FLOAT x = 0, FLOAT y = 0,
                                                   FLOAT angle = 0,
                                                   FLOAT scale = 1,
                                                   FLOAT r = 1, FLOAT g = 1,
                                                   FLOAT b = 1, FLOAT a = 1);

extern void hls2rgb(FLOAT *r, FLOAT *g, FLOAT *b, FLOAT h, FLOAT l, FLOAT s);


#endif // !GFX_H_INCL
