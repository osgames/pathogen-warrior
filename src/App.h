/* Pathogen Warrior
 * Copyright 2004 Jetro Lauha - http://iki.fi/jetro/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: App.h,v 1.5 2004/07/14 15:37:09 tonic Exp $
 * $Revision: 1.5 $
 */

#ifndef APP_H_INCL
#define APP_H_INCL


#include <assert.h>


enum APP_STATE
{
    AS_UNINITIALIZED    = 0,
    // must be last:
    APP_STATE_COUNT
};


class State;


class App
{
public:

    typedef std::list<SDLKey> KeyEventList;

    App();
    virtual ~App();

    static void setInstance(App *singleton);
    static App * getInstance()
    {
        assert(mSingleton != NULL);
        return mSingleton;
    }

    virtual BOOL init();

    virtual void run();

    virtual void setState(INT state);
    APP_STATE getState() const { return mState; }

    void setAlive(BOOL alive) { mAlive = alive; }
    BOOL isAlive() const { return mAlive; }

    SDL_Surface * getScreen() const { return mScreen; }

    void setKeyState(UINT8 *keyState) { mKeyState = keyState; }
    UINT8 * getKeyState() const { return mKeyState; }

    KeyEventList & getKeyEventList() { return mKeyEvents; }

protected:

    virtual BOOL update();

    void resetTime();

    virtual char * getCaption() = 0;
    virtual State * getStateHandler(INT state) = 0;

    static App *mSingleton;

    APP_STATE mState;

    SDL_Surface *mScreen;
    BOOL mAlive;
    UINT32 mTime, mStartTime;
    UINT32 mNextUpdateTime;

    KeyEventList mKeyEvents;
    UINT8 *mKeyState;

    State *mCurrentStateHandler;
    SDL_EventFilter mCustomEventFilter;
};


#endif // !APP_H_INCL
