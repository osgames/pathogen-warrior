/* Pathogen Warrior
 * Copyright 2004 Jetro Lauha - http://iki.fi/jetro/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: TitleState.cpp,v 1.2 2004/07/14 15:37:09 tonic Exp $
 * $Revision: 1.2 $
 */

#include "main.h"


BOOL TitleState::update(UINT32 time, App::KeyEventList &keyEvents)
{
    mTime = time;

    keyEvents.clear();

    return TRUE;
}


void TitleState::render(SDL_Surface *screen)
{
    clear(0, 0, 0);
    enter2DMode(WIDTH, HEIGHT);

    GameApp *app = (GameApp *)App::getInstance();
    Font *font = app->getFont();

    INT rh = font->getFontHeight();
    INT x = WIDTH / 2;
    INT y = HEIGHT / 8;
    font->drawString(x + 1, y + 2, FONT_ALIGN_HORIZ_CENTER, "Pathogen Warrior",
                     sin(mTime * 0.005f) * 0.25f + 0.25f, 0, 0);
    font->drawString(x, y, FONT_ALIGN_HORIZ_CENTER, "Pathogen Warrior");

    y += rh;
    font->drawString(x, y, FONT_ALIGN_HORIZ_CENTER, "version " VERSION_STR, 0.2f, 0.2f, 0.3f);

    y += rh * 2;
    font->drawString(x, y, FONT_ALIGN_HORIZ_CENTER,
                     "Horrid pandemic plagues are threatening the world!", 0.8f, 0.6f, 0.6f);

    y += rh * 2;
    font->drawString(x, y, FONT_ALIGN_HORIZ_CENTER,
                     "You are the Pathogen Warrior.", 0.8f, 0.6f, 0.6f);
    y += rh;
    font->drawString(x, y, FONT_ALIGN_HORIZ_CENTER,
                     "Your task is to save the world.", 0.8f, 0.6f, 0.6f);

    y += rh * 2;
    font->drawString(x, y, FONT_ALIGN_HORIZ_CENTER,
                     "Match the pathogens to hex-vaccines. New sample of the pathogen can be", 0.5f, 0.5f, 0.7f);
    y += rh;
    font->drawString(x, y, FONT_ALIGN_HORIZ_CENTER,
                     "fetched by clicking the problem spot on the world map. Drag with mouse", 0.5f, 0.5f, 0.7f);
    y += rh;
    font->drawString(x, y, FONT_ALIGN_HORIZ_CENTER,
                     "to move the compounds on the hex map. You can create a link by clicking", 0.5f, 0.5f, 0.7f);
    y += rh;
    font->drawString(x, y, FONT_ALIGN_HORIZ_CENTER,
                     "an empty hexagon or rotate a link by clicking an existing link.", 0.5f, 0.5f, 0.7f);

    y += rh * 2;
    BuildState *bs = app->getBuildState();
    UINT32 score = bs->getScore();
    UINT32 pathogenCount = bs->getPathogenCount();
    if (score > 0)
    {
        char str[256];
        sprintf(str, "Pathogen %u ended last game. Score: %u points.", pathogenCount, score);
        font->drawString(x, y, FONT_ALIGN_HORIZ_CENTER, str, 0.5f, 0.9f, 0.5f);
    }

    y += rh * 2;
    font->drawString(x, y, FONT_ALIGN_HORIZ_CENTER, "Press a key or click with a mouse to start a new game.");

    leave2DMode();
}


void TitleState::onKeyDown(const SDLKey &key)
{
    gameStart();
}


void TitleState::onMouseDown(const SDL_MouseButtonEvent &event)
{
    gameStart();
}


void TitleState::gameStart()
{
    GameApp *app = (GameApp *)App::getInstance();
    if (app->isAlive())
        app->setState(GS_BUILD);
}
