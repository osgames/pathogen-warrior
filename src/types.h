/* Pathogen Warrior
 * Copyright 2004 Jetro Lauha - http://iki.fi/jetro/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: types.h,v 1.3 2004/07/14 15:37:09 tonic Exp $
 * $Revision: 1.3 $
 */

#ifndef TYPES_H_INCLUDED
#define TYPES_H_INCLUDED

#undef FLOAT
#undef INT8
#undef UINT8
#undef INT16
#undef UINT16
#undef INT32
#undef UINT32
#undef BOOL
#undef TRUE
#undef FALSE
#define FLOAT float
#define INT8 signed char
#define UINT8 unsigned char
#define INT16 signed short
#define UINT16 unsigned short
#define INT32 int
#define UINT32 unsigned int
#define INT int
#define UINT unsigned int
#define BOOL bool
#define TRUE true
#define FALSE false

#undef NULL
// valid for C++
#define NULL 0

#endif // !TYPES_H_INCLUDED
