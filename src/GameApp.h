/* Pathogen Warrior
 * Copyright 2004 Jetro Lauha - http://iki.fi/jetro/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: GameApp.h,v 1.7 2004/07/14 15:37:09 tonic Exp $
 * $Revision: 1.7 $
 */

#ifndef GAMEAPP_H_INCL
#define GAMEAPP_H_INCL


#include "App.h"


#define CAPTION_STR "Pathogen Warrior"


enum GAME_STATE
{
    // start game states where base app states end
    GS_TITLE    = APP_STATE_COUNT,
    GS_BUILD
};


class Font;
class BuildState;
class TitleState;


class GameApp : public App
{
public:

    GameApp();
    virtual ~GameApp();

    virtual BOOL init();

    Font * getFont() { return mFont; }

    BuildState * getBuildState() { return mBuildState; }

protected:

    virtual char * getCaption();
    virtual State * getStateHandler(INT state);

    BuildState *mBuildState;
    TitleState *mTitleState;

    Font *mFont;
};


#endif // !GAMEAPP_H_INCL
