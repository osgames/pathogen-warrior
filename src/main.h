/* Pathogen Warrior
 * Copyright 2004 Jetro Lauha - http://iki.fi/jetro/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: main.h,v 1.13 2004/07/14 18:24:23 tonic Exp $
 * $Revision: 1.13 $
 */

#ifndef MAIN_H_INCL
#define MAIN_H_INCL


#include <list>
#include <map>
#include <vector>
#include <set>
#include <string>
#include "SDL.h"
#include "SDL_opengl.h"
#include "types.h"
#include "errormsg.h"
#include "gfx.h"
#include "sound.h"
#include "App.h"
#include "GameApp.h"
#include "Font.h"
#include "State.h"
#include "BuildState.h"
#include "TitleState.h"


#define WIDTH       800
#define HEIGHT      600

#define VERSION_STR "1.1.1"


#undef PI
#define PI 3.1415926535897932


#endif // !MAIN_H_INCL
